# Npad - a simple note bash script

Copyright (C) 2021 Adriel Sand

```
This program is free software: you can redistribute it 
and/or modify it under the terms of the GNU General 
Public License as published by the Free Software Foundation, 
either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be 
useful, but WITHOUT ANY WARRANTY; without even the implied 
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public 
License along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

To run:
```sh
git clone https://gitlab.com/thebiblelover7/npad
cd npad
chmod +x npad
./npad
```

You can also move the script to your $PATH and then you can run it from anywhere

## Contributing

PRs and issues are greatly appreciated! You can check out the todo list below for ideas.

## Todo

- Be able to select the note you want to open
- Show note content preview
